package cs108;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.List;

public final class LZWInputStream extends InputStream {
    private final static int MAX_BITS = 16, MAX_CAPACITY = 1 << MAX_BITS;
    private final BitsInputStream in;
    private final List<List<Integer>> dict;
    private final Deque<Integer> currList;
    private List<Integer> prevList;

    public LZWInputStream(InputStream in) {
        List<List<Integer>> dict = new ArrayList<>(MAX_CAPACITY);
        for (int i = 0; i <= 0xFF; ++i)
            dict.add(List.of(i));

        this.in = new BitsInputStream(in);
        this.dict = dict;
        this.currList = new ArrayDeque<>();
        this.prevList = null;
    }

    @Override
    public int read() throws IOException {
        if (currList.isEmpty()) {
            boolean addEntry = dict.size() < MAX_CAPACITY && prevList != null;
            int futureDictSize = dict.size() + (addEntry ? 1 : 0);
            int bits = 32 - Integer.numberOfLeadingZeros(futureDictSize - 1);
            int i = in.readU(bits);
            if (i == -1)
                return -1;

            List<Integer> newList = i < dict.size()
                    ? dict.get(i) : copyAndAppendHead(prevList, prevList);

            if (addEntry)
                dict.add(copyAndAppendHead(prevList, newList));

            currList.addAll(newList);
            prevList = newList;
        }
        return currList.removeFirst();
    }

    private <T> List<T> copyAndAppendHead(List<T> l, List<T> l2) {
        List<T> r = new ArrayList<>(l.size() + 1);
        r.addAll(l);
        r.add(l2.get(0));
        return Collections.unmodifiableList(r);
    }

    @Override
    public void close() throws IOException {
        in.close();
    }
}
