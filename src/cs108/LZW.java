package cs108;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public final class LZW {
    public static void main(String[] args) throws IOException {
        String inFileName = args[0];
        String outCmpFileName = args[1];
        String outFileName = args[2];

        try (InputStream in = new FileInputStream(inFileName)) {
            try (OutputStream out = new LZWOutputStream(
                    new FileOutputStream(outCmpFileName))) {
                copyStream(in, out);
            }
        }

        try (InputStream in = new LZWInputStream(
                new FileInputStream(outCmpFileName))) {
            try (OutputStream out = new FileOutputStream(outFileName)) {
                copyStream(in, out);
            }
        }
    }

    private static void copyStream(InputStream in, OutputStream out)
            throws IOException {
        int b;
        while ((b = in.read()) != -1)
            out.write(b);
    }
}
