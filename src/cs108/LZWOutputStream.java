package cs108;

import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public final class LZWOutputStream extends OutputStream {
    private final static int MAX_BITS = 16, MAX_CAPACITY = 1 << MAX_BITS;

    private final BitsOutputStream out;
    private final Map<List<Integer>, Integer> dict;
    private final List<Integer> prefix;

    public LZWOutputStream(OutputStream out) {
        Map<List<Integer>, Integer> dict = new HashMap<>(MAX_CAPACITY);
        for (int i = 0; i <= 0xFF; ++i)
            dict.put(Collections.singletonList(i), i);

        this.out = new BitsOutputStream(out);
        this.dict = dict;
        this.prefix = new ArrayList<>();
    }

    @Override
    public void write(int b0) throws IOException {
        int b = b0 & 0xFF;

        prefix.add(b);
        if (dict.containsKey(prefix))
            return;

        List<Integer> choppedPrefix = prefix.subList(0, prefix.size() - 1);
        writeIndex(dict.get(choppedPrefix));

        if (dict.size() < MAX_CAPACITY) {
            List<Integer> immutablePrefix =
                Collections.unmodifiableList(new ArrayList<>(prefix));
            dict.put(immutablePrefix, dict.size());
        }
        choppedPrefix.clear();
    }

    @Override
    public void close() throws IOException {
        if (!prefix.isEmpty())
            writeIndex(dict.get(prefix));
        out.close();
    }

    private void writeIndex(int v) throws IOException {
        out.writeU(v, 32 - Integer.numberOfLeadingZeros(dict.size() - 1));
    }
}
