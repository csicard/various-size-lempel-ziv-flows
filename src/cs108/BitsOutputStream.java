package cs108;

import java.io.IOException;
import java.io.OutputStream;

public final class BitsOutputStream implements AutoCloseable {
    private final OutputStream out;
    private int waitingBits, waitingBitsCount;

    public BitsOutputStream(OutputStream out) {
        this.out = out;
        this.waitingBits = 0;
        this.waitingBitsCount = 0;
    }

    public void writeU(int v, int count) throws IOException {
        if (! (0 <= count && count <= 16))
            throw new IllegalArgumentException("illegal bits count: " + count);
        if (! (0 <= v && v < (1 << count)))
            throw new IllegalArgumentException("too many bits");

        waitingBits = (waitingBits << count) | v;
        waitingBitsCount += count;

        while (waitingBitsCount >= 8) {
            out.write(waitingBits >> (waitingBitsCount - 8));
            waitingBitsCount -= 8;
        }
    }

    @Override
    public void close() throws IOException {
        if (waitingBitsCount > 0) {
            out.write(waitingBits << (8 - waitingBitsCount));
            waitingBitsCount = 0;
        }
        out.close();
    }
}
