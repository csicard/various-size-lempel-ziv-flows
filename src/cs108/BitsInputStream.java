package cs108;

import java.io.IOException;
import java.io.InputStream;

public final class BitsInputStream implements AutoCloseable {
    private final InputStream in;
    private int waitingBits, waitingBitsCount;

    public BitsInputStream(InputStream in) {
        this.in = in;
        this.waitingBits = 0;
        this.waitingBitsCount = 0;
    }

    public int readU(int count) throws IOException {
        if (! (0 <= count && count <= 16))
            throw new IllegalArgumentException();

        while (waitingBitsCount < count) {
            int b = in.read();
            if (b == -1)
                return -1;
            waitingBits = (waitingBits << 8) | b;
            waitingBitsCount += 8;
        }

        waitingBitsCount -= count;
        int mask = (1 << count) - 1;
        return (waitingBits >> waitingBitsCount) & mask;
    }

    @Override
    public void close() throws IOException {
        in.close();
    }
}
